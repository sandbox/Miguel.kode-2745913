AGILE REST module for Drupal 8.x.
This module provides a form, which you can send data to AGILE CRM.

INTRODUCTION
------------

This module provides a custom form, which send data to a AGILE CRM account.


REQUIREMENTS
------------

You need a AGILE CRM acount. The data that you need are:

 - The domain of your account.
 - The user email of your account.
 - The REST API Key of your account.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.


CONFIGURATION
-------------

 * Configure the domain, user email and REST API key of your AGILE CRM account at admin/content/agile_rest/settings.


MAINTAINERS
-----------

Current maintainers:

 * Miguel Ángel Caro García.
