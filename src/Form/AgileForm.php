<?php

namespace Drupal\agile_rest\Form;


use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


/**
 * Class AgileForm.
 *
 * @package Drupal\agile_rest\Form
 */
class AgileForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'agile_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'agile_rest.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['agile_crm_domain'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => \Drupal::state()->get('agile_crm_domain'),
      '#description' => $this->t('This is the domain of Agile CRM'),
      '#maxlength' => 64,
      '#size' => 64,
    );
    $form['agile_crm_user_email'] = array(
      '#type' => 'email',
      '#title' => $this->t('User email'),
      '#default_value' => \Drupal::state()->get('agile_crm_user_email'),
      '#description' => $this->t('This is the email form the CRM user'),
      '#maxlength' => 64,
      '#size' => 64,
    );
    $form['agile_crm_rest_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('REST API Key'),
      '#default_value' => \Drupal::state()->get('agile_crm_rest_api_key'),
      '#description' => $this->t('This is the key of Agile CRM'),
      '#maxlength' => 64,
      '#size' => 64,
    );
    $form['accept'] = array(
      '#type' => 'submit',
      '#title' => $this->t('Accept'),
      '#value' => t('Accept'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::state()->set('agile_crm_domain', $form['agile_crm_domain']['#value']);
    \Drupal::state()->set('agile_crm_user_email', $form['agile_crm_user_email']['#value']);
    \Drupal::state()->set('agile_crm_rest_api_key', $form['agile_crm_rest_api_key']['#value']);

  }

}
