<?php


namespace Drupal\agile_rest\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

module_load_include('inc', 'agile_rest', 'inc/curlwrap_v2');
/**
 * Class ContactCrmForm.
 *
 * @package Drupal\agile_rest\Form
 */
class ContactCrmForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_crm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 64,
      '#size' => 64,
      '#placeholder' => $this->t('Name'),
    );
    $form['last_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Surname'),
      '#maxlength' => 64,
      '#size' => 64,
      '#placeholder' => $this->t('Surname'),
    );
    $form['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#placeholder' => $this->t('Email'),
    );
    $form['accept'] = array(
      '#type' => 'submit',
      '#title' => $this->t('Send'),
      '#value' => t('Send'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $contact_json = array(
      "lead_score" => "0",
      "star_value" => "5",
      "tags" => array(),
      "properties" => array(
        array(
          "name" => "first_name",
          "value" => $form['name']['#value'],
          "type" => "SYSTEM",
        ),
        array(
          "name" => "last_name",
          "value" => $form['last_name']['#value'],
          "type" => "SYSTEM",
        ),
        array(
          "name" => "email",
          "value" => $form['email']['#value'],
          "type" => "SYSTEM",
        ),
      ),
    );

    $contact_json = json_encode($contact_json);
    agile_crm_curl_wrap("contacts", $contact_json, "POST", "application/json");
    // Set message confirmation.
    $message = "Your message has been sent";
    drupal_set_message($message);

    return TRUE;
  }

}
