<?php

namespace Drupal\agile_rest\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'FormContact' block.
 *
 * @Block(
 *  id = "form_contact",
 *  admin_label = @Translation("Form contact"),
 * )
 */
class FormContact extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['form_contact']['form'] = \Drupal::formBuilder()->getForm('Drupal\agile_rest\Form\ContactCrmForm');

    return $build;
  }

}
